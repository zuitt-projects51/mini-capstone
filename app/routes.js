const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
	
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'Message': 'Bad request. Property name is missing!'
			})
		}

		if(typeof(req.body.name) != 'string'){
			return res.status(400).send({
				'Message': 'Bad request.  Name should be string!'
			})
		}

		if(req.body.name == ''){
			return res.status(400).send({
				'Message': 'Bad request.  Name should not be empty!'

			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'Message': 'Bad request.  Exchange is missing'
			})
		}

		if(typeof(req.body.ex) != 'object'){
			return res.status(400).send({
				'Message': 'Bad request. Exchange should be an object'
			})
		}

		if(Object.entries(req.body.ex).length === 0){

			return res.status(400).send({
				'Message': 'Bad request. Exchange should not be empty'
			})

		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'Message': 'Bad request. Property alias is missing'
			})
		}

		if(typeof(req.body.alias) != 'string'){
			return res.status(400).send({
				'Message':'Bad request. Property alias should be a string'
			})
		}

		if(req.body.alias == ''){
			return res.status(400).send({
				'Message': 'Bad request. Property alias should not be empty'
			})
		}

		for (i = 0; i< Object.keys(exchangeRates).length; i++) {
			if (Object.keys(exchangeRates)[i] === req.body.alias) {
				}
			else{
				continue;
			}

			return res.status(400).send({
				'Message': 'Bad request. Duplicate alias'
			})
		}

		return res.status(200).send({
			'Message': 'Route is successfully running'
		})
	})
}