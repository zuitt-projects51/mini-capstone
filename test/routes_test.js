const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001'

	it('GET / endpoint', (done)=>{
		chai.request(domain).get('/').end((error,res)=>{
			expect(res.status).to.equal(200);
			done();
		})
	})


	it('POST /currency endpoint is running with status 200 and all fields are complete', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(200);
			done()
		})
	})

	it('POST /currency endpoint name is missing', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			address: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint name is not a string', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: true,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})
	
	it('POST /currency endpoint name is empty', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: '',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint ex is missing', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh'
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint ex is not an object', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: 'sar'
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint ex is empty', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint alias is missing', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			code: 'pound',
			name: 'United Kingdom',
			ex: {
				'peso': 70.03,
		        'usd': 1.38,
		        'won': 1621.90,
		        'yuan':8.82
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint alias is not a string', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 1,
			name: 'United Kingdom',
			ex: {
				'peso': 70.03,
		        'usd': 1.38,
		        'won': 1621.90,
		        'yuan':8.82
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})	
	
	it('POST /currency endpoint alias is empty', (done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: '',
			name: 'United Kingdom',
			ex: {
				'peso': 70.03,
		        'usd': 1.38,
		        'won': 1621.90,
		        'yuan':8.82
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('POST /currency endpoint alias is duplicate',(done)=>{
		chai.request(domain).post('/currency').type('json')
		.send({
			alias: 'usd',
			name: 'United Kingdom',
			ex: {
				'peso': 70.03,
		        'usd': 1.38,
		        'won': 1621.90,
		        'yuan':8.82
			}
		})
		.end((error,res)=>{
			expect(res.status).to.equal(400);
			done()
		})
	})

})